package soapExample;

import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.xml.soap.*;
import javax.activation.*;
import javax.servlet.http.*;
import java.util.*;

public class ch18_04 extends HttpServlet
{
    private SOAPConnection connection;

    public void init(ServletConfig servletConfig) throws ServletException
    {
        super.init(servletConfig);

        try {
        SOAPConnectionFactory connectionFactory =
                    SOAPConnectionFactory.newInstance();
            connection = connectionFactory.createConnection();
        } catch(Exception e) {}
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException
    {
        String outString =   "<HTML><H1>Sending and reading the SOAP Message test</H1><P>";

        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage outgoingMessage = messageFactory.createMessage();

            SOAPPart soappart = outgoingMessage.getSOAPPart();
            SOAPEnvelope envelope = soappart.getEnvelope();
            SOAPHeader header = envelope.getHeader();
            SOAPBody body = envelope.getBody();

            body.addBodyElement(envelope.createName("numberAvailable",
            "laptops",
            "http://www.XMLPowerCorp.com")).addTextNode("216");

            StringBuffer serverUrl = new StringBuffer();
            serverUrl.append(request.getScheme()).append("://").
                append(request.getServerName());
            serverUrl.append(":").append(request.getServerPort()).
                append(request.getContextPath());
            
            String baseUrl = serverUrl.toString();
            

            URL client = new URL(baseUrl + "/ch18_05");
            FileOutputStream outgoingFile = new FileOutputStream("/home/tomcat/uploads/out.msg");
            outgoingMessage.writeTo(outgoingFile);
            outgoingFile.close();

            outString += "<P>SOAP outgoingMessage sent (see out.msg).</P> <BR>";
            SOAPMessage incomingMessage = connection.call(outgoingMessage, client);
            
            if (incomingMessage != null) {
                FileOutputStream incomingFile = new FileOutputStream("/home/tomcat/uploads/in.msg");
                incomingMessage.writeTo(incomingFile);
                incomingFile.close();
                outString +=
                    "SOAP outgoingMessage received (see in.msg).</HTML>";
            }

        } catch(Throwable e) {
            outString += e.toString();
        }

        try {
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(outString.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {}
    }
}