package multicast;

import java.io.IOException;

import multicast.network.message.Message;

/**
 * Sender is an interface for sending a Message to nodes in the Total Order
 * Broadcast mode. In-order, reiable delivery to all nodes is guaranteed, unless
 * an exception is thrown.
 * 
 * @author Chase Covello Philip Russell
 * @see Total Order Broadcast and Multicast Algorithms: Taxonomy and Survery,
 *      Defago et al
 * @see Section 4.1, pages 12 - 13
 */
public interface Sender {

	/**
	 * Total Order Broadcast the given message to nodes in the multicast group.
	 * 
	 * @param message
	 *            the message to broadcast.
	 * @throws IOException
	 *             in the case of unrecoverable network errors.
	 */
	public void totalOrderBroadcast(Message message) throws IOException;
}