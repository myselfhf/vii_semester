package multicast.util;

import java.util.Random;

/**
 * This class contains miscellaneous utility functions for the network code.
 * 
 * @author Chase Covello Philip Russell
 * 
 */
public class Network {

	private static final Random g = new Random();

	/**
	 * Returns a uniformly distributed random long value. Since longs are 64
	 * bit, collisions happen with probability 2^-32.
	 * 
	 * @return a random long.
	 */
	public static long getRandomLong() {
		return ((long) g.nextInt() << 32) ^ g.nextInt();
	}
}