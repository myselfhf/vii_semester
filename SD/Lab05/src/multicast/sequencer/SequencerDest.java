package multicast.sequencer;

import java.io.IOException;
import java.net.SocketAddress;

import multicast.Dest;
import multicast.handlers.MessageHandler;
import multicast.handlers.NullMessageHandler;
import multicast.network.MulticastMessageReceiver;

/**
 * A Sequencer Destination receives Messages from the network and delivers them
 * to the given message handler.
 * 
 * @author Chase Covello Philip Russell
 * @see Total Order Broadcast and Multicast Algorithms: Taxonomy and Survery,
 *      Defago et al
 * @see Section 4.1, pages 12 - 13
 * 
 */
public class SequencerDest extends MulticastMessageReceiver implements Dest {

	/**
	 * Construct a new SequencerDest with the given multicast manager and a
	 * no-op message handler.
	 * 
	 * @param multicastManager
	 *            the multicast manager to join.
	 * @throws IOException
	 *             in the case of unrecoverable network errors.
	 */
	public SequencerDest(SocketAddress multicastManager) throws IOException {
		this(multicastManager, new NullMessageHandler());
	}

	/**
	 * Construct a new SequencerDest with the given mulicast manager and message
	 * handler.
	 * 
	 * @param multicastManager
	 *            the multicast manager to join.
	 * @param handler
	 *            the message handler to use.
	 * @throws IOException
	 *             in the case of unrecoverable network errors.
	 */
	public SequencerDest(SocketAddress multicastManager, MessageHandler handler)
			throws IOException {
		super(multicastManager, handler);
	}
}