package multicast.sequencer;

import java.net.SocketAddress;

/**
 * A Sequencer is a process that acts as an intermediary between a Sender
 * and a Destination. It imposes a total order on the messages transmitted
 * between them.
 * 
 * @author Chase Covello Philip Russell
 * @see Total Order Broadcast and Multicast Algorithms: Taxonomy and Survery,
 *      Defago et al
 * @see Section 4.1, pages 12 - 13
 * 
 */public interface Sequencer {

	 /**
	  * Returns the socket address this Sequencer lestens for messages on.
	  *  
	  * @return the socket address.
	  */
	public SocketAddress getSocketAddress();
	
	/**
	 * Shuts down any threads created by this sequencer. This method <b>must</b>
	 * be called before disposing of all references to this object; otherwise,
	 * its thread will continue to run until the application is forcibly
	 * terminated.
	 */
	public void shutdown();
}