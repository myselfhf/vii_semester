package multicast.sequencer;


import java.io.IOException;
import java.net.SocketAddress;
import java.net.SocketException;

import multicast.Sender;
import multicast.network.message.Message;

/**
 * A SequencerSender receives a Message from the layer above, and transmits it
 * to some other group of processes across a communications network. Typically,
 * it transmits the message to a group of Sequencers accross the Internet.
 * 
 * @see Total Order Broadcast and Multicast Algorithms: Taxonomy and Survery,
 *      Defago et al
 * @see Section 4.1, pages 12 - 13
 * 
 * @author Chase Covello Philip Russell
 * 
 */
public class SequencerSender implements Sender {

	private final SocketAddress[] sequencers;

	/**
	 * Primary constructor.
	 * 
	 * @param sequencers
	 *            The array of communcations portals for some receiving process,
	 *            usually a Sequencer.
	 * @throws SocketException
	 *             Thrown if the interal message delivery mechanism cannot be
	 *             instantiated.
	 */
	public SequencerSender(SocketAddress... sequencers) {
		this.sequencers = sequencers;
	}

	/**
	 * Primary method for this class, attempts to send a Message to some group
	 * of receiving processes.
	 * 
	 * @throws IOException
	 *             Thrown if sending the message accross the communications
	 *             channel fails.
	 */
	public void totalOrderBroadcast(Message message) throws IOException {
		for (SocketAddress sequencer : sequencers)
			message.send(sequencer);
	}
}