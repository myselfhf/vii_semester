package multicast;

import multicast.handlers.MessageHandler;

/**
 * Dest is an interface for receiving a Message in the Total Order Broadcast
 * mode.
 * 
 * @author Chase Covello Philip Russell
 * @see Total Order Broadcast and Multicast Algorithms: Taxonomy and Survery,
 *      Defago et al
 * @see Section 4.1, pages 12 - 13
 */
public interface Dest {

	/**
	 * Sets the message handler to be used for this Dest. A message handler is a
	 * type of event handler called whenever a message arrives at this Dest.
	 * 
	 * @param handler
	 *            the message handler to use.
	 */
	public void setMessageHandler(MessageHandler handler);

	/**
	 * Shuts down any threads created by this Dest. This method <b>must</b> be
	 * called before disposing of all references to this object; otherwise, its
	 * thread will continue to run until the application is forcibly terminated.
	 */
	public void shutdown();
}