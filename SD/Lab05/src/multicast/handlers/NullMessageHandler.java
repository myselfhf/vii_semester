package multicast.handlers;


import multicast.network.message.Message;

/**
 * NullMessageHandler receives messages from the network and discards them.
 * 
 * @author Chase Covello Philip Russell
 * 
 */
public class NullMessageHandler implements MessageHandler {

	/**
	 * Receives a message and immediately returns.
	 */
	public void receive(Message message) {
	}
}