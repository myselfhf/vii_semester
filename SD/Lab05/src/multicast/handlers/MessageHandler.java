package multicast.handlers;

import multicast.network.message.Message;

/**
 * MessageHandler is an interface that provides event handling when a message is
 * received. An instance of a classes that implements this interface can be
 * passed to MessageReceiver and its subclasses. When a message arrives, the
 * receive method is invoked on the incoming message.
 * 
 * @author Chase Covello Philip Russell
 * 
 */
public interface MessageHandler {

	/**
	 * This method is called when a message arrives from the network. Possible
	 * actions include printing the message to the console, enqueuing the
	 * message, or passing it to a class that builds upon the functionality of
	 * MessageReceiver.
	 * 
	 * @param message
	 *            the Message that just arrived.
	 */
	public void receive(Message message);
}