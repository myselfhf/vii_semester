package multicast.network.message;

import java.io.Serializable;
import java.net.SocketAddress;

/**
 * An ObjectMessage is a message that contains a serializable object for
 * transfer over the network.
 * 
 * @author Chase Covello Philip Russell
 * 
 */
public class ObjectMessage extends Message {

	private static final long serialVersionUID = 6L;

	private final Serializable object;

	/**
	 * Constructs a new ObjectMessage using the given message type, object, and
	 * source address.
	 * 
	 * @param type
	 *            the message type.
	 * @param object
	 *            the object.
	 * @param source
	 *            the source of this message.
	 */
	public ObjectMessage(MessageType type, Serializable object,
			SocketAddress source) {
		super(type, source);

		this.object = object;
	}

	/**
	 * Constructs a new ObjectMessage using information from the given
	 * ObjectMessage.
	 * 
	 * @param m
	 *            the ObjectMessage to copy.
	 */
	public ObjectMessage(ObjectMessage m) {
		super(m);

		this.object = m.object;
	}

	/**
	 * Returns this message's object.
	 * 
	 * @return the object.
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * Returns a hash code for this ObjectMessage. The hash code is given by the
	 * formula <code>super.hashCode() ^ object.hashCode()</code>.
	 */
	public int hashCode() {
		return super.hashCode() ^ object.hashCode();
	}

	/**
	 * Returns a string representation of this ObjectMessage.
	 */
	public String toString() {
		return super.toString() + ", object=" + object;
	}
}
