#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

int lazarDado(){
    srand(time(NULL));
    return (rand() % 6)+1;
}

pthread_mutex_t sema1,sema2;
pthread_mutexattr_t semattr1,semattr2;
int N;
void *jugador1(void *arg){
    int i = N;
    while(i-->0){
        pthread_mutex_lock(&sema1);
        printf("Jugador 1 obtuvo: %d\n", lazarDado());
        sleep(1);
        pthread_mutex_unlock(&sema2);
    }
}
void *jugador2(void *arg){
    int i = N;
    while(i-->0){
        pthread_mutex_lock(&sema2);
        printf("Jugador 2 obtuvo: %d\n", lazarDado());
        sleep(1);
        pthread_mutex_unlock(&sema1);
    }
}
int main(){

    pthread_t tid1, tid2;
    pthread_attr_t attr;
    N = 10;
    pthread_mutexattr_init(&semattr1);
    pthread_mutexattr_init(&semattr2);
    if(pthread_mutex_init(&sema1,&semattr1)==-1){
        perror("El semaforo 1 no ha podido inicializarse\n");
        exit(1);
    }

    if(pthread_mutex_init(&sema2,&semattr2)==-1){
        perror("El semaforo 2 no ha podido inicializarse\n");
        exit(1);
    }

    if(pthread_attr_init(&attr)==-1){
        perror("Error en inicializacion de los atributos de un thread \n");
        exit(1);
    }
    pthread_mutex_lock(&sema2);

    if(pthread_create(&tid1,&attr,jugador1,NULL)==-1){
        perror("Error en la creación del hilo 1 \n");
        exit(1);
    }

    if(pthread_create(&tid2,&attr,jugador2,NULL)==-1){
        perror("Error en la creación del hilo 2 \n");
        exit(1);
    }

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    printf("Salida del hilo principal\n");
    exit(0);
}