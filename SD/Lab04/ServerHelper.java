import java.net.*;
import java.io.*;

public class ServerHelper implements Runnable{
	private byte bufferEntrada[];
	DatagramPacket dp;
	DatagramSocket ds;

	ServerHelper(DatagramSocket ds, DatagramPacket dp, byte[] buffer){
		this.ds = ds;
		this.dp =  dp;
		this.bufferEntrada = buffer;
	}

	public void run(){
		
		try{
			Thread.sleep(1000);
			int puerto = dp.getPort();
	
			// Dirección de Internet desde donde se envió
			InetAddress direcc = dp.getAddress();
	
			// "Envolvemos" el buffer con un ByteArrayInputStream...
			ByteArrayInputStream bais = new ByteArrayInputStream(bufferEntrada);
	
			// ... que volvemos a "envolver" con un DataInputStream
			DataInputStream dis = new DataInputStream(bais);
	
			// Y leemos un número entero a partir del array de bytes
			int entrada = dis.readInt();
			long salida = (long)entrada*(long)entrada;
	
			// Creamos un ByteArrayOutputStream sobre el que podamos escribir
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
	
			// Lo envolvemos con un DataOutputStream
			DataOutputStream dos = new DataOutputStream(baos);
	
			// Escribimos el resultado, que debe ocupar 8 bytes
			dos.writeLong(salida);
	
			// Cerramos el buffer de escritura
			dos.close();
	
			// Generamos el paquete de vuelta, usando los datos
			// del remitente del paquete original
			dp = new
			DatagramPacket(baos.toByteArray(),8,direcc,puerto);
	
			// Enviamos
			ds.send(dp);
	
			// Registramos en salida estandard
			System.out.println( "Cliente = " + direcc + ":" + puerto + "\tEntrada = " + entrada + "\tSalida = " + salida );
		}catch(Exception e){
			System.err.println("Se ha producido el error " + e);
		}
	}
}