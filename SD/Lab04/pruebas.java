public class pruebas{
	public static void main(String[] args) {
		int arr[] = criba(60);
		for (int i = 0; i < arr.length ;i++ ) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		
	}
	public static int[] criba(int end){
		int nums[] = new int[end + 1];
		for(int i=0; i <= end; i++){
			nums[i] = i;
		}
		int noPrimes = 0;
		int count = 2;
		while(nums[count]*nums[count] <= end){
			if(nums[count]!=0){
				for(int i = 2; i <= end/nums[count];i++){
					if(nums[count*i]!=0)
						noPrimes++;
					nums[count*i] = 0;
					
				}
			}
			count++;
		}
		int output[] = new int[end - noPrimes];
		int j = 0;
		for (int i = 0; i < nums.length ; i++ ) {
			if(nums[i] != 0)
				output[j++] = nums[i];
		}
		return output;
	}
}