//clienteUDP.java

import java.net.*;
import java.io.*;
import java.util.*;

class clienteUDP{

  public static void main(String args[]){

    // Leemos el primer parámetro, donde debe ir la dirección
    // IP del servidor
    InetAddress direcc = null;

    try{
      direcc = InetAddress.getByName(args[0]);
    }catch(UnknownHostException uhe){
      System.err.println("Host no encontrado :" + uhe);
      System.exit(-1);
    }
    // Puerto que hemos usado para el servidor
    int puerto = 1234;

    // Creamos el Socket
    DatagramSocket ds = null;

    try{
      ds = new DatagramSocket();
    }catch(SocketException se){
      System.err.println("Error al abrir el socket :" + se);
      System.exit(-1);
    }

    
    sendArrayInt(ds, direcc, puerto, criba(Integer.parseInt(args[1])));
    /*for (int n=1;n < args.length;n++){
      try{
        //creamos un buffer para escribir
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);

        // Convertimos el texto en número
        int numero = Integer.parseInt(args[n]);

        // Lo escribimos
        dos.writeInt(numero);

        // y cerramos el buffer
        dos.close();

        // Creamos paquete
        DatagramPacket dp = new DatagramPacket(baos.toByteArray(),4,direcc,puerto);

        // y lo mandamos
        ds.send(dp);

        // Preparamos buffer para recibir número de 8 bytes
        byte bufferEntrada[] = new byte[8];

        // Creamos el contenedor del paquete
        dp = new DatagramPacket(bufferEntrada,8);
        // y lo recibimos
        ds.receive(dp);

        // Creamos un stream de lectura a partir del buffer
        ByteArrayInputStream bais = new ByteArrayInputStream(bufferEntrada);
        DataInputStream dis = new DataInputStream(bais);

        // Leemos el resultado final
        long resultado = dis.readLong();

        // Indicamos en pantalla
        System.out.println("Solicitud = " + numero + "\tResultado = " + resultado );
      }catch(Exception e){
        System.err.println("Se ha producido un error : " + e);
      }

    }*/

  }

  public static void sendArrayInt(DatagramSocket ds, InetAddress direcc, int puerto, int []values){
    for (int n=0;n < values.length; n++){
      try{
        //creamos un buffer para escribir
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);

        // Convertimos el texto en número
        int numero = values[n];

        // Lo escribimos
        dos.writeInt(numero);

        // y cerramos el buffer
        dos.close();

        // Creamos paquete
        DatagramPacket dp = new DatagramPacket(baos.toByteArray(),4,direcc,puerto);

        // y lo mandamos
        ds.send(dp);

        // Preparamos buffer para recibir número de 8 bytes
        byte bufferEntrada[] = new byte[8];

        // Creamos el contenedor del paquete
        dp = new DatagramPacket(bufferEntrada,8);
        // y lo recibimos
        ds.receive(dp);

        // Creamos un stream de lectura a partir del buffer
        ByteArrayInputStream bais = new ByteArrayInputStream(bufferEntrada);
        DataInputStream dis = new DataInputStream(bais);

        // Leemos el resultado final
        long resultado = dis.readLong();

        // Indicamos en pantalla
        System.out.println("Solicitud = " + numero + "\tResultado = " + resultado );
      }catch(Exception e){
        System.err.println("Se ha producido un error : " + e);
      }

    }
  }
  public static int[] criba(int end){
    int nums[] = new int[end + 1];
    for(int i=0; i <= end; i++){
      nums[i] = i;
    }
    int noPrimes = 0;
    int count = 2;
    while(nums[count]*nums[count] <= end){
      if(nums[count]!=0){
        for(int i = 2; i <= end/nums[count];i++){
          if(nums[count*i]!=0)
            noPrimes++;
          nums[count*i] = 0;
          
        }
      }
      count++;
    }
    int output[] = new int[end - noPrimes];
    int j = 0;
    for (int i = 0; i < nums.length ; i++ ) {
      if(nums[i] != 0)
        output[j++] = nums[i];
    }
    return output;
  }
}
