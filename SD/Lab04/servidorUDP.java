import java.net.*;
import java.io.*;

class servidorUDP{
	
	public static void main(String args[]){

		// Primero indicamos la dirección IP local
		try{
			System.out.println("LocalHost = " + InetAddress.getLocalHost().toString());
		}catch (UnknownHostException uhe){
			System.err.println("No puedo saber la dirección IP local " + uhe);
		}
		// Abrimos un Socket UDP en el puerto 1234.
		// A través de este Socket enviaremos datagramas del tipo DatagramPacket

		DatagramSocket ds = null;
		try{
			ds = new DatagramSocket(1234);
		}catch(SocketException se){
			System.err.println("Se ha producido un error al abrir el socket :" + se);
			System.exit(-1);

		}

		// Bucle infinito
		while(true){
			try{

				// Nos preparamos a recibir un número entero (32 bits = 4 bytes)
				byte bufferEntrada[] = new byte[4];

				// Creamos un "contenedor" de datagrama, cuyo buffer
				// será el array creado antes
				DatagramPacket dp = new DatagramPacket(bufferEntrada,4);

				// Esperamos a recibir un paqete
				ds.receive(dp);
				(new Thread(new ServerHelper(ds, dp, bufferEntrada))).start();


			}catch(Exception e){
				System.err.println("Se ha producido el error " + e);
			}

		}

	}

}
