import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
public class RemoteController{
	Socket sc;
	ObjectOutputStream out;
	ObjectInputStream in;
	String argsend[] = new String[1];
	RemoteController(String host, int port){
		try {
			System.out.println("Connectando...");
			sc = new Socket(host, port);
			System.out.println("Connectado a "+host+":"+port );
			out = new ObjectOutputStream(sc.getOutputStream());
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Object lookup(String service, ArrayList<ServiceArguments> arguments) throws ClassNotFoundException, IOException{
		System.out.println("Buscando servicio");
		NameService sr = new NameService(service,arguments);
		out.writeObject(sr);
		out.flush();
		in = new ObjectInputStream(sc.getInputStream());
		return in.readObject();
	}
}
