import java.io.Serializable;
import java.util.ArrayList;

class NameService implements Serializable{
	ArrayList<ServiceArguments>arguments;
	String serviceName;
	NameService(String name, ArrayList<ServiceArguments> args){
		serviceName = name;
		arguments = args;
	}
	String getName(){
		return serviceName;
	}
	ArrayList<ServiceArguments> getArgs(){
		return arguments;
	}
}