import java.io.* ;
import java.net.* ;
import java.util.HashMap;
import java.util.ArrayList;


public class ServerController{
	HashMap<String, RemoteObject>services = new HashMap<String, RemoteObject>();
	ServerSocket serverAddr = null;
	Socket  sc = null;
	ObjectOutputStream out;
	ObjectInputStream in;

	ServerController(int port){
		try {
			serverAddr = new ServerSocket(2500);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void init(){
		while(true){
			try {
				System.out.println("Esperando por coneccion...");
				sc = serverAddr.accept();
				System.out.println("coneccion hecha...");
				System.out.println("Esperando entradas...");
				InputStream istream = sc.getInputStream();
				ObjectInput in = new ObjectInputStream(istream);
				String service[];
				System.out.println("esperando lectura..");
				NameService  sr = (NameService)in.readObject();
				System.out.println("Nueva entrada ... :" + sr.getName());
				RemoteObject rmObject = services.get(sr.getName());
				rmObject.setArguments(sr.getArgs());
				Object result = rmObject.run();
				ObjectOutputStream ostream = new ObjectOutputStream(sc.getOutputStream());
				ostream.writeObject(result);
				ostream.flush();
				sc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}catch(ClassNotFoundException e){
				e.printStackTrace();
			}
		}
	}

	public void registryService(String name, RemoteObject object){
		services.put(name, object);
	}

}
