
import java.io.* ;
import java.util.ArrayList;


public class Client
{
	public static void main ( String [] args) throws ClassNotFoundException, IOException {
		RemoteController cliente = new RemoteController("localhost", 2500);
		ServiceArguments<Integer>arg1 = new ServiceArguments<Integer>(1);
		ServiceArguments<Integer>arg2 = new ServiceArguments<Integer>(6);
		ArrayList<ServiceArguments>arguments = new ArrayList<ServiceArguments>();
		arguments.add(arg1);
		arguments.add(arg2);
		Object valor = cliente.lookup("suma", arguments).toString();
		System.out.println("Resultado :" +valor);
	}
}