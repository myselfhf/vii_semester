import java.util.ArrayList;

public interface RemoteObject {
	ArrayList<ServiceArguments>arguments = null;
	public Object run();
	public void setArguments(ArrayList<ServiceArguments> ser);
}
