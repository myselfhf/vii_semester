import java.io.FileNotFoundException;

public class DOM {
	
	private NodoInstruccionProcesador[] inst;
	private Documento document;
	
	public DOM(NodoInstruccionProcesador[] ins, Documento doc) {
		inst = ins;
		document = doc;
	}
	
	/*private DOM(String input) throws FileNotFoundException {
		initObjetos(input);
	}
	
	
	private void initObjetos(String input) throws FileNotFoundException {
		inst = DOMFactory.getInstruccionesProcesador(input);
		doc = DOMFactory.getModel(input);
	}*/

	public Documento getDocumentObjectModel(String input) throws FileNotFoundException { 
		return document;
	}
	
	public NodoInstruccionProcesador[] getInstuccionesProcesador() {
		return inst;
	}
	
}
