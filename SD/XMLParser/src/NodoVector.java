public interface NodoVector {
	public Nodo item(int index);
    public int getLength();
}
