import java.util.ArrayList;

public interface Nodo {

	public final static int NODO_ELEMENTO = 1;
	public final static int NODO_ATRIBUTO = 2;
	public final static int NODO_TEXTO = 3;
	public final static int NODO_DOCUMENTO = 4;
	public static final int NODO_INSTRUCCION_PROCESADOR = 5;
	
	public int getTipoNodo();
	public String getNombre();
	public String getValor() throws XMLException;
	public void setValor(String val) throws XMLException;
	public Nodo getNodoPadre();
	public NodoVector getNodosHijos() throws XMLException;
	public NodoVector getAtributos() throws XMLException;
	public void reemplazarHijo(Nodo anterior, Nodo nuevo);
	public void eliminarHijo(Nodo hijo);
	public void addHijo(Nodo hijo) throws XMLException;
	public boolean tieneHijos();
	public boolean tieneAtributos();
	public boolean esElMismoNodo(Nodo otro);
	public void setPadre(Nodo nodoPadre) throws XMLException;
}
