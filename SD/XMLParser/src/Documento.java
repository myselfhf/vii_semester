
public class Documento implements Nodo{
	private int tipo = Nodo.NODO_DOCUMENTO;
	
	private NodoVectorImpl hijos;
	private NodoVectorImpl atrib;
	private String nombre;
	private Nodo padre;
	
	public Documento(String name) {
		nombre = name;
		padre = this;
		hijos = new NodoVectorImpl();
		atrib = new NodoVectorImpl();
	}
	
	public int getTipoNodo() {
		return tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getValor() throws XMLException {
		throw new XMLException();		
	}

	public void setValor(String v) throws XMLException {
		throw new XMLException();
	}

	public Nodo getNodoPadre() {
		return padre;
	}

	public NodoVector getNodosHijos() {
		return hijos;
	}

	public NodoVector getAtributos() {
		return atrib;
	}

	public void reemplazarHijo(Nodo anterior, Nodo nuevo) {
		
	}

	public void eliminarHijo(Nodo hijo) {
		hijos.eliminar(hijo);		
	}

	public void addHijo(Nodo hijo) throws XMLException {
		hijo.setPadre(this);
		hijos.addNodo(hijo);
	}
	
	public void setPadre(Nodo nodoPadre) throws XMLException {
		throw new XMLException();
	}

	public boolean tieneHijos() {
		return hijos.getLength() == 0 ? false : true;
	}

	public boolean tieneAtributos() {
		return atrib.getLength() == 0 ? false : true;
	}

	public boolean esElMismoNodo(Nodo otro) {
		return (this == otro);
	}
}
