import java.util.ArrayList;

public class NodoVectorImpl implements NodoVector {
	private ArrayList<Nodo> nodos = new ArrayList<>();
	
	public NodoVectorImpl(Nodo ... n) {
	  for(Nodo i : n)
		  nodos.add(i);
	}
	
	public Nodo item(int index) {
		return nodos.get(index);
	}

	@Override
	public int getLength() {
		return nodos.size();
	}
	
	public void addNodo(Nodo n) {
		nodos.add(n);
	}

	public void eliminar(Nodo nodo) {
		for (Nodo n : nodos) {
			if (n.esElMismoNodo(nodo))
				nodos.remove(n);
		}
	}
}
