
public class NodoInstruccionProcesador implements Nodo {

	private int tipo = Nodo.NODO_INSTRUCCION_PROCESADOR;
	
	private String nombre;
	private String valor;
	private Nodo padre;
	
	public NodoInstruccionProcesador(String name, String val) {
		nombre = name;
		padre = this;
		valor = val;
	}
	
	public NodoInstruccionProcesador(String name) {
		this(name, null);
	}
	
	
	public int getTipoNodo() {
		return tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getValor() throws XMLException {
		return valor;		
	}

	public void setValor(String v) throws XMLException {
		valor = v;
	}

	public Nodo getNodoPadre() {
		return padre;
	}

	public NodoVector getNodosHijos() throws XMLException {
		throw new XMLException();
	}

	public NodoVector getAtributos() throws XMLException {
		throw new XMLException();
	}

	public void reemplazarHijo(Nodo anterior, Nodo nuevo) {
		
	}

	public void eliminarHijo(Nodo hijo) {
				
	}

	public void addHijo(Nodo hijo) throws XMLException {
		throw new XMLException();
	}
	
	public void setPadre(Nodo nodoPadre) {
		this.padre = nodoPadre;
	}

	public boolean tieneHijos() {
		return false;
	}

	public boolean tieneAtributos() {
		return false;
	}

	public boolean esElMismoNodo(Nodo otro) {
		return (this == otro);
	}

}
