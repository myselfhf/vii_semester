
public class Atributo implements Nodo{
	private int tipo = Nodo.NODO_ATRIBUTO;
	
	private String nombre;
	private Nodo padre;
	private String valor;
	
	public Atributo(String name) {
		this(name, null);
	}
	
	public Atributo(String name, String valor) {
		nombre = name;
		padre = this;
		this.valor = valor;
	}
	
	
	public int getTipoNodo() {
		return tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getValor() throws XMLException {
		return valor;		
	}

	public void setValor(String v) {
		valor = v;
	}

	public Nodo getNodoPadre() {
		return padre;
	}

	public NodoVector getNodosHijos() {
		return null;
	}

	public NodoVector getAtributos() {
		return null;
	}

	public void reemplazarHijo(Nodo anterior, Nodo nuevo) {
		
	}

	public void eliminarHijo(Nodo hijo) {
				
	}

	public void addHijo(Nodo hijo) {
		
	}
	
	public void setPadre(Nodo nodoPadre) {
		this.padre = nodoPadre;
	}

	public boolean tieneHijos() {
		return false;
	}

	public boolean tieneAtributos() {
		return false;
	}

	public boolean esElMismoNodo(Nodo otro) {
		return (this == otro);
	}

}
