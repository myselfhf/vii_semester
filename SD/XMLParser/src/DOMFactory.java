import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.StringTokenizer;
import java.util.*;

public class DOMFactory {
    //private Documento root;
	private static String regexIns = "(\\Q<?\\E)(.*)(\\Q?>\\E)";
	private static String regexDoc = "(\\Q<\\E)([^\\Q?\\E]+)(\\Q>\\E)(.*)(\\Q</\\E)(.*)(\\Q>\\E)";
	private static String tag = "bookstoree";
	private static String regexTag = "(\\Q<" + tag + "\\E)(\\s.*)(\\Q>\\E)(.*)(\\Q</" + tag + ">\\E)";
	private static String regexTag2 = "((\\Q<\\E)([^\\Q?\\E]+)(\\Q>\\E)(.*)(\\Q</\\E)(.+)(\\Q>\\E))+";
	private static Pattern p_nip = Pattern.compile(regexIns);
	private static Pattern p_doc = Pattern.compile(regexDoc);
	private static Pattern p_tag = Pattern.compile(regexTag2);
	//private static Matcher m = p.compile(regexIns);
	private static DOM parse(String path) throws FileNotFoundException {
		String docTex = copyFromFile(path);
		NodoInstruccionProcesador[] nip = getNodosInstruccionProcesador(docTex);
		Documento doc = getDocument(docTex);
		
		return new DOM(nip, doc);
	}

	private static Documento getDocument(String tex) {
		String doc = "";
		//String regex  = "<\\/?\\w+(\\s[.[^<>]]+)?>";
		//String regex  = "(<\\w+(\\s[.[^<>]]+)?>) (.*) (<\\/\\w+(\\s[.[^<>]]+)?>)";
		String firstTag  = "(<\\w+(\\s[.[^<>]]+)?>)";
		Pattern patter =  Pattern.compile(firstTag);
		Matcher m = patter.matcher(tex);
		String firstTagName = "";
		if(m.find()){
			firstTagName = m.group(1);
		}
		System.out.println("primera = "+ firstTagName);
		String intoTagReg = firstTagName + ".*" + "</" + firstTagName.substring(1);
		patter = Pattern.compile(intoTagReg);
		String intoTag = "";
		m = patter.matcher(tex);
		if(m.find()){
			System.out.println(m.group(1));
		}
		/*while(m.find()){
			doc = m.group();
			System.out.println("-----" +doc);
		}
		System.out.println(doc);*/

		return null;
		/*String[] ins = instrucciones_proc.split("\\s");
		NodoInstruccionProcesador[] nip = new NodoInstruccionProcesador[ins.length];
		for (int i = 0; i < ins.length; ++i) {
			String[] temp = ins[i].split("=");
			if (temp.length == 2)
				nip[i] = new NodoInstruccionProcesador(temp[0], temp[1]);
			else
				nip[i] = new NodoInstruccionProcesador(temp[0]);
		}
		return nip;*/
	}

	private static NodoInstruccionProcesador[] getNodosInstruccionProcesador(String tex) {
		String instrucciones_proc = "";
		
		Matcher m = p_nip.matcher(tex);
		while(m.find())
			instrucciones_proc = m.group(2);
		
		String[] ins = instrucciones_proc.split("\\s");
		NodoInstruccionProcesador[] nip = new NodoInstruccionProcesador[ins.length];
		for (int i = 0; i < ins.length; ++i) {
			String[] temp = ins[i].split("=");
			if (temp.length == 2)
				nip[i] = new NodoInstruccionProcesador(temp[0], temp[1]);
			else
				nip[i] = new NodoInstruccionProcesador(temp[0]);
		}
		return nip;
	}

	private static String copyFromFile(String path) throws FileNotFoundException {
		Scanner sc = new Scanner(new BufferedReader(new FileReader(path)));
		
		String temp = "";
		while (sc.hasNextLine()) {
            temp += sc.nextLine();
            //tex += "\n";
        }
		
		sc.close();
		return temp;
	}

	public static void main(String sgs[]){ 
		try {
			DOMFactory.parse("bookstore.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static NodoInstruccionProcesador getInstruccionesProcesador(String input) {
		// TODO Auto-generated method stub
		return null;
	}
}
