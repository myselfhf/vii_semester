public class DOMSample{
	public static void main(String[] args) {
		try{
			if (argv.length != 1){
            // Must pass in the name of the XML file.
				System.err.println("Usage: java DOMSample filename");
				System.exit(1);
			}
			
         // Generate a URL from the filename.
			URL url = DemoUtil.createURL(argv[0]);


			DOMParser parser = new DOMParser();
			parser.setErrorStream(System.err);
			parser.setValidationMode(DOMParser.DTD_VALIDATION);
			parser.showWarnings(true);
			parser.parse(url);
			System.out.print("The elements are: ");
			printElements(doc);
			XMLDocument doc = parser.getDocument();
			System.out.println("The attributes of each element are: ");
			printElementAttributes(doc);

		}catch(Exceprion e){
			
		}
		
	}
	static void printElements(Document doc){
		NodeList nl = doc.getElementsByTagName("*");
		Node n;

		for (int i=0; i<nl.getLength(); i++)
		{
			n = nl.item(i);
			System.out.print(n.getNodeName() + " ");
		}

		System.out.println();
	}

	static void printElementAttributes(Document doc){
		NodeList nl = doc.getElementsByTagName("*");
		Element e;
		Node n;
		NamedNodeMap nnm;

		String attrname;
		String attrval;
		int i, len;

		len = nl.getLength();

		for (int j=0; j < len; j++)
		{
			e = (Element)nl.item(j);
			System.out.println(e.getTagName() + ":");
			nnm = e.getAttributes();

			if (nnm != null)
			{
				for (i=0; i<nnm.getLength(); i++)
				{
					n = nnm.item(i);
					attrname = n.getNodeName();
					attrval = n.getNodeValue();
					System.out.print(" " + attrname + " = " + attrval);
				}
			}
			System.out.println();
		}
	}
}