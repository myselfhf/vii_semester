import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.imageio.ImageIO;
import java.util.Hashtable;
import helma.xmlrpc.*;

public class JavaServer {

    public JavaServer () {
        // Our handler is a regular Java object. It can have a
        // constructor and member variables in the ordinary fashion.
        // Public methods will be exposed to XML-RPC clients.
    }

    public Hashtable sumAndDifference (int x, int y) {
        Hashtable result = new Hashtable();
        result.put("sum", new Integer(x + y));
        result.put("difference", new Integer(x - y));
        return result;
    }

    //method to call from a client
    public Hashtable multimedia(String imageName, String videoName) throws IOException{
        Hashtable result = new Hashtable();
        result.put("imagen", encodeImage(".", imageName));
        return result;
    }

    //encode a image in a array of bytes
    private byte[] encodeImage(String dirName, String imageName) throws IOException{
        ByteArrayOutputStream baos=new ByteArrayOutputStream(1000);
        BufferedImage img=ImageIO.read(new File(dirName,imageName));
        ImageIO.write(img, "jpg", baos);
        baos.flush();
        baos.close();
        return baos.toByteArray();
    }
    

    public static void main (String [] args) {
        try {

            // Invoke me as <http://localhost:8080/RPC2>.
            WebServer server = new WebServer(8080);
            server.addHandler("sample", new JavaServer());

        } catch (Exception exception) {
            System.err.println("JavaServer: " + exception.toString());
        }
    }
}