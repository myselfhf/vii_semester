import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.imageio.ImageIO;

public class test{
	public static void main(String[] args) {
		try{
			String dirName=".";
			ByteArrayOutputStream baos=new ByteArrayOutputStream(1000);
			BufferedImage img=ImageIO.read(new File(dirName,"imagen.jpg"));
			ImageIO.write(img, "jpg", baos);
			baos.flush();
			baos.close();
	 
			byte[] bytearray = baos.toByteArray();
	 
			BufferedImage imag=ImageIO.read(new ByteArrayInputStream(bytearray));
			ImageIO.write(imag, "jpg", new File(dirName,"snap.jpg"));
		}catch(Exception e){
			System.err.println("JavaClient: " + e.toString());
		}
	}
}