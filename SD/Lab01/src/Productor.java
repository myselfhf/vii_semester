class Productor extends Thread { 
	private CubbyHole cubbyhole;
	private int numero;
	public Productor(CubbyHole c, int numero) {
		cubbyhole = c;
		this.numero = numero;
	}
	public void run() {
		for (int i = 0; i < 10; i++) {
			cubbyhole.put(i);
			System.out.println("Productor #"+this.numero+"pone: " +i);
			try {
				sleep((int)(Math.random() * 100));
			} catch (InterruptedException e) {
				
			}
		}
	}
	public static void main(String[] args) {
		CubbyHole cubby = new CubbyHole();
		Productor productor = new Productor(cubby, 1);
		Consumidor consumidor1 = new Consumidor(cubby, 1);
		Consumidor consumidor2 = new Consumidor(cubby, 2);
		productor.start();
		consumidor1.start();
		consumidor2.start();
	}
}