import java.applet.*;
import java.awt.*;

public class HelloWorldApplet extends Applet
{
	int count;
	Contar alto,bajo;
	public void init(){
		count = 9;
		bajo = new Contar( 200 );
		// El otro comienza desde cero alto = new Contar( 0 );
		// Al que comienza en 200 le asignamos prioridad mínima bajo.setPriority( Thread.MIN_PRIORITY );
		// Y al otro máxima
		alto.setPriority( Thread.MAX_PRIORITY );
		//System.out.println( "Prioridad alta es"+alto.getPriority() );
		System.out.println( "Prioridad baja es "+bajo.getPriority() );
	}
   public void paint (Graphics g)
   {
      g.drawString ("Hello World"+count, 25, 50);
   }
}