import java.util.Vector;
import java.util.Hashtable;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import helma.xmlrpc.*;

public class JavaClient {

  // The location of our server.
  private final static String server_url =
  "http://localhost:8080/RPC2";
  public BufferedImage encodeImage(String dirName, String imageName) throws IOException{
        BufferedImage img = ImageIO.read(new File(dirName,imageName));
        return img;
    }
  public static void main (String [] args) {
    try {

      // Create an object to represent our server.
      XmlRpcClient server = new XmlRpcClient(server_url);

      // Build our parameter list.
      Vector params = new Vector();
      params.addElement(new String("imagen.jpg"));
      params.addElement(new String("video.avi"));

      // Call the server, and get our result.
      Hashtable result = (Hashtable) server.execute("sample.multimedia", params);
      byte[] bytearray = (byte[])result.get("imagen");
      

      // write the imagen downloaded.
      System.out.println("Download...");
      BufferedImage imag=ImageIO.read(new ByteArrayInputStream(bytearray));
      ImageIO.write(imag, "jpg", new File(".","nuewImage.jpg"));


    } catch (XmlRpcException exception) {
      System.err.println("JavaClient: XML-RPC Fault #" +
        Integer.toString(exception.code) + ": " +
        exception.toString());
    } catch (Exception exception) {
      System.err.println("JavaClient: " + exception.toString());
    }
  }
}
