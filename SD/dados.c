#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

int lazarDado(){
    return (rand() % (6 + 1 - 1)) + 1;
}

int N;
void *tarea1(void *arg){
    int i = N;
    while(i-->0){
        printf("Jugador 1 obtuvo: %d\n", lazarDado());
    }
}
void *tarea2(void *arg){
    int i = N;
    while(i-->0){
        int dado = (rand() % (6 + 1 - 1)) + 1;
        printf("Jugador 2 obtuvo: %d\n", lazarDado());
    }
}
int main(){

    pthread_t tid1, tid2;
    pthread_attr_t attr;
    N = 10;

    if(pthread_attr_init(&attr)==-1){
        perror("Error en inicializacion de los atributos de un thread \n");
        exit(1);
    }

    if(pthread_create(&tid1,&attr,tarea1,NULL)==-1){
        perror("Error en la creación del hilo 1 \n");
        exit(1);
    }

    if(pthread_create(&tid2,&attr,tarea2,NULL)==-1){
        perror("Error en la creación del hilo 2 \n");
        exit(1);
    }

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    printf("Salida del hilo principal\n");
    exit(0);
}